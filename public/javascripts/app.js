(function(/*! Brunch !*/) {
  'use strict';

  var globals = typeof window !== 'undefined' ? window : global;
  if (typeof globals.require === 'function') return;

  var modules = {};
  var cache = {};

  var has = function(object, name) {
    return ({}).hasOwnProperty.call(object, name);
  };

  var expand = function(root, name) {
    var results = [], parts, part;
    if (/^\.\.?(\/|$)/.test(name)) {
      parts = [root, name].join('/').split('/');
    } else {
      parts = name.split('/');
    }
    for (var i = 0, length = parts.length; i < length; i++) {
      part = parts[i];
      if (part === '..') {
        results.pop();
      } else if (part !== '.' && part !== '') {
        results.push(part);
      }
    }
    return results.join('/');
  };

  var dirname = function(path) {
    return path.split('/').slice(0, -1).join('/');
  };

  var localRequire = function(path) {
    return function(name) {
      var dir = dirname(path);
      var absolute = expand(dir, name);
      return globals.require(absolute);
    };
  };

  var initModule = function(name, definition) {
    var module = {id: name, exports: {}};
    definition(module.exports, localRequire(name), module);
    var exports = cache[name] = module.exports;
    return exports;
  };

  var require = function(name) {
    var path = expand(name, '.');

    if (has(cache, path)) return cache[path];
    if (has(modules, path)) return initModule(path, modules[path]);

    var dirIndex = expand(path, './index');
    if (has(cache, dirIndex)) return cache[dirIndex];
    if (has(modules, dirIndex)) return initModule(dirIndex, modules[dirIndex]);

    throw new Error('Cannot find module "' + name + '"');
  };

  var define = function(bundle) {
    for (var key in bundle) {
      if (has(bundle, key)) {
        modules[key] = bundle[key];
      }
    }
  }

  globals.require = require;
  globals.require.define = define;
  globals.require.brunch = true;
})();

window.require.define({"application": function(exports, require, module) {
  var Application, Chaplin, HeaderView, Layout, mediator, routes,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Chaplin = require('chaplin');

  mediator = require('mediator');

  routes = require('routes');

  Layout = require('views/layout');

  HeaderView = require('views/header_view');

  module.exports = Application = (function(_super) {

    __extends(Application, _super);

    function Application() {
      return Application.__super__.constructor.apply(this, arguments);
    }

    Application.prototype.title = 'Art Shaped Box';

    Application.prototype.initialize = function() {
      Application.__super__.initialize.apply(this, arguments);
      this.initDispatcher();
      this.initLayout();
      this.initMediator();
      this.initRouter(routes, {
        pushState: false
      });
      this.header = new HeaderView;
      return this.header.render();
    };

    Application.prototype.initLayout = function() {
      return this.layout = new Layout({
        title: this.title
      });
    };

    Application.prototype.initMediator = function() {
      Chaplin.mediator.user = null;
      return Chaplin.mediator.seal();
    };

    return Application;

  })(Chaplin.Application);
  
}});

window.require.define({"controllers/base/controller": function(exports, require, module) {
  var Chaplin, Controller,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Chaplin = require('chaplin');

  module.exports = Controller = (function(_super) {

    __extends(Controller, _super);

    function Controller() {
      return Controller.__super__.constructor.apply(this, arguments);
    }

    return Controller;

  })(Chaplin.Controller);
  
}});

window.require.define({"controllers/home_controller": function(exports, require, module) {
  var AboutPageView, ArtistPageView, CalendarPageView, ContactPageView, Controller, HomeController, HomePageView, IndividualPageView, LocationPageView,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Controller = require('controllers/base/controller');

  HomePageView = require('views/home_page_view');

  ArtistPageView = require('views/artist_page_view');

  ContactPageView = require('views/contact_page_view');

  LocationPageView = require('views/location_page_view');

  AboutPageView = require('views/about_page_view');

  CalendarPageView = require('views/calendar_page_view');

  IndividualPageView = require('views/individual_page_view');

  module.exports = HomeController = (function(_super) {

    __extends(HomeController, _super);

    HomeController.prototype.historyURL = '';

    function HomeController() {
      HomeController.__super__.constructor.apply(this, arguments);
    }

    HomeController.prototype.index = function() {
      return this.view = new HomePageView();
    };

    HomeController.prototype.artists = function() {
      return this.view = new ArtistPageView();
    };

    HomeController.prototype.contacts = function() {
      return this.view = new ContactPageView();
    };

    HomeController.prototype.location = function() {
      return this.view = new LocationPageView();
    };

    HomeController.prototype.about = function() {
      return this.view = new AboutPageView();
    };

    HomeController.prototype.calendar = function() {
      return this.view = new CalendarPageView();
    };

    HomeController.prototype.individual = function(params) {
      return this.view = new IndividualPageView(params.name);
    };

    return HomeController;

  })(Controller);
  
}});

window.require.define({"initialize": function(exports, require, module) {
  var Application;

  Application = require('application');

  $(function() {
    var app;
    app = new Application();
    return app.initialize();
  });
  
}});

window.require.define({"lib/services/service_provider": function(exports, require, module) {
  var Chaplin, ServiceProvider, utils;

  utils = require('lib/utils');

  Chaplin = require('chaplin');

  module.exports = ServiceProvider = (function() {

    _(ServiceProvider.prototype).extend(Chaplin.Subscriber);

    ServiceProvider.prototype.loading = false;

    function ServiceProvider() {
      _(this).extend($.Deferred());
      utils.deferMethods({
        deferred: this,
        methods: ['triggerLogin', 'getLoginStatus'],
        onDeferral: this.load
      });
    }

    ServiceProvider.prototype.disposed = false;

    ServiceProvider.prototype.dispose = function() {
      if (this.disposed) {
        return;
      }
      this.unsubscribeAllEvents();
      this.disposed = true;
      return typeof Object.freeze === "function" ? Object.freeze(this) : void 0;
    };

    return ServiceProvider;

  })();

  /*

    Standard methods and their signatures:

    load: ->
      # Load a script like this:
      utils.loadLib 'http://example.org/foo.js', @loadHandler, @reject

    loadHandler: =>
      # Init the library, then resolve
      ServiceProviderLibrary.init(foo: 'bar')
      @resolve()

    isLoaded: ->
      # Return a Boolean
      Boolean window.ServiceProviderLibrary and ServiceProviderLibrary.login

    # Trigger login popup
    triggerLogin: (loginContext) ->
      callback = _(@loginHandler).bind(this, loginContext)
      ServiceProviderLibrary.login callback

    # Callback for the login popup
    loginHandler: (loginContext, response) =>

      eventPayload = {provider: this, loginContext}
      if response
        # Publish successful login
        mediator.publish 'loginSuccessful', eventPayload

        # Publish the session
        mediator.publish 'serviceProviderSession',
          provider: this
          userId: response.userId
          accessToken: response.accessToken
          # etc.

      else
        mediator.publish 'loginFail', eventPayload

    getLoginStatus: (callback = @loginStatusHandler, force = false) ->
      ServiceProviderLibrary.getLoginStatus callback, force

    loginStatusHandler: (response) =>
      return unless response
      mediator.publish 'serviceProviderSession',
        provider: this
        userId: response.userId
        accessToken: response.accessToken
        # etc.
  */

  
}});

window.require.define({"lib/support": function(exports, require, module) {
  var Chaplin, support, utils;

  Chaplin = require('chaplin');

  utils = require('lib/utils');

  support = utils.beget(Chaplin.support);

  module.exports = support;
  
}});

window.require.define({"lib/utils": function(exports, require, module) {
  var Chaplin, mediator, utils,
    __hasProp = {}.hasOwnProperty;

  Chaplin = require('chaplin');

  mediator = require('mediator');

  utils = Chaplin.utils.beget(Chaplin.utils);

  _(utils).extend({
    /*
      Wrap methods so they can be called before a deferred is resolved.
      The actual methods are called once the deferred is resolved.
    
      Parameters:
    
      Expects an options hash with the following properties:
    
      deferred
        The Deferred object to wait for.
    
      methods
        Either:
        - A string with a method name e.g. 'method'
        - An array of strings e.g. ['method1', 'method2']
        - An object with methods e.g. {method: -> alert('resolved!')}
    
      host (optional)
        If you pass an array of strings in the `methods` parameter the methods
        are fetched from this object. Defaults to `deferred`.
    
      target (optional)
        The target object the new wrapper methods are created at.
        Defaults to host if host is given, otherwise it defaults to deferred.
    
      onDeferral (optional)
        An additional callback function which is invoked when the method is called
        and the Deferred isn't resolved yet.
        After the method is registered as a done handler on the Deferred,
        this callback is invoked. This can be used to trigger the resolving
        of the Deferred.
    
      Examples:
    
      deferMethods(deferred: def, methods: 'foo')
        Wrap the method named foo of the given deferred def and
        postpone all calls until the deferred is resolved.
    
      deferMethods(deferred: def, methods: def.specialMethods)
        Read all methods from the hash def.specialMethods and
        create wrapped methods with the same names at def.
    
      deferMethods(
        deferred: def, methods: def.specialMethods, target: def.specialMethods
      )
        Read all methods from the object def.specialMethods and
        create wrapped methods at def.specialMethods,
        overwriting the existing ones.
    
      deferMethods(deferred: def, host: obj, methods: ['foo', 'bar'])
        Wrap the methods obj.foo and obj.bar so all calls to them are postponed
        until def is resolved. obj.foo and obj.bar are overwritten
        with their wrappers.
    */

    deferMethods: function(options) {
      var deferred, func, host, methods, methodsHash, name, onDeferral, target, _i, _len, _results;
      deferred = options.deferred;
      methods = options.methods;
      host = options.host || deferred;
      target = options.target || host;
      onDeferral = options.onDeferral;
      methodsHash = {};
      if (typeof methods === 'string') {
        methodsHash[methods] = host[methods];
      } else if (methods.length && methods[0]) {
        for (_i = 0, _len = methods.length; _i < _len; _i++) {
          name = methods[_i];
          func = host[name];
          if (typeof func !== 'function') {
            throw new TypeError("utils.deferMethods: method " + name + " notfound on host " + host);
          }
          methodsHash[name] = func;
        }
      } else {
        methodsHash = methods;
      }
      _results = [];
      for (name in methodsHash) {
        if (!__hasProp.call(methodsHash, name)) continue;
        func = methodsHash[name];
        if (typeof func !== 'function') {
          continue;
        }
        _results.push(target[name] = utils.createDeferredFunction(deferred, func, target, onDeferral));
      }
      return _results;
    },
    createDeferredFunction: function(deferred, func, context, onDeferral) {
      if (context == null) {
        context = deferred;
      }
      return function() {
        var args;
        args = arguments;
        if (deferred.state() === 'resolved') {
          return func.apply(context, args);
        } else {
          deferred.done(function() {
            return func.apply(context, args);
          });
          if (typeof onDeferral === 'function') {
            return onDeferral.apply(context);
          }
        }
      };
    }
  });

  module.exports = utils;
  
}});

window.require.define({"lib/view_helper": function(exports, require, module) {
  var mediator, utils;

  mediator = require('mediator');

  utils = require('chaplin/lib/utils');

  Handlebars.registerHelper('if_logged_in', function(options) {
    if (mediator.user) {
      return options.fn(this);
    } else {
      return options.inverse(this);
    }
  });

  Handlebars.registerHelper('with', function(context, options) {
    if (!context || Handlebars.Utils.isEmpty(context)) {
      return options.inverse(this);
    } else {
      return options.fn(context);
    }
  });

  Handlebars.registerHelper('without', function(context, options) {
    var inverse;
    inverse = options.inverse;
    options.inverse = options.fn;
    options.fn = inverse;
    return Handlebars.helpers["with"].call(this, context, options);
  });

  Handlebars.registerHelper('with_user', function(options) {
    var context;
    context = mediator.user || {};
    return Handlebars.helpers["with"].call(this, context, options);
  });

  Handlebars.registerHelper('src_prefix', function() {
    return 'http://da91.amber.feralhosting.com/';
  });
  
}});

window.require.define({"mediator": function(exports, require, module) {
  
  module.exports = require('chaplin').mediator;
  
}});

window.require.define({"models/base/collection": function(exports, require, module) {
  var Chaplin, Collection,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Chaplin = require('chaplin');

  module.exports = Collection = (function(_super) {

    __extends(Collection, _super);

    function Collection() {
      return Collection.__super__.constructor.apply(this, arguments);
    }

    return Collection;

  })(Chaplin.Collection);
  
}});

window.require.define({"models/base/model": function(exports, require, module) {
  var Chaplin, Model,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Chaplin = require('chaplin');

  module.exports = Model = (function(_super) {

    __extends(Model, _super);

    function Model() {
      return Model.__super__.constructor.apply(this, arguments);
    }

    return Model;

  })(Chaplin.Model);
  
}});

window.require.define({"models/header": function(exports, require, module) {
  var Header, Model,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Model = require('models/base/model');

  module.exports = Header = (function(_super) {

    __extends(Header, _super);

    function Header() {
      return Header.__super__.constructor.apply(this, arguments);
    }

    Header.prototype.defaults = {
      items: [
        {
          href: './test/',
          title: 'App Tests'
        }, {
          href: 'http://brunch.readthedocs.org/',
          title: 'Docs'
        }, {
          href: 'https://github.com/brunch/brunch/issues',
          title: 'Github Issues'
        }, {
          href: 'https://github.com/paulmillr/ostio',
          title: 'Devin'
        }
      ]
    };

    return Header;

  })(Model);
  
}});

window.require.define({"models/user": function(exports, require, module) {
  var Model, User,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Model = require('models/base/model');

  module.exports = User = (function(_super) {

    __extends(User, _super);

    function User() {
      return User.__super__.constructor.apply(this, arguments);
    }

    return User;

  })(Model);
  
}});

window.require.define({"routes": function(exports, require, module) {
  
  module.exports = function(match) {
    match('', 'home#index');
    match('about', 'home#about');
    match('home', 'home#index');
    match('location', 'home#location');
    match('calendar', 'home#calendar');
    match('artist/:name', 'home#individual');
    match('artists', 'home#artists');
    return match('contact', 'home#contacts');
  };
  
}});

window.require.define({"views/about_page_view": function(exports, require, module) {
  var AboutPageView, PageView, template,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  template = require('views/templates/about');

  PageView = require('views/base/page_view');

  module.exports = AboutPageView = (function(_super) {

    __extends(AboutPageView, _super);

    function AboutPageView() {
      return AboutPageView.__super__.constructor.apply(this, arguments);
    }

    AboutPageView.prototype.template = template;

    AboutPageView.prototype.className = 'about-page';

    AboutPageView.prototype.templateData = {
      info: ["Art Shaped Box is a club for UCLA students who want to practice and explore visual art in different mediums, such as painting, illustration, photography, sculpture, digital art, installation art, guerilla art, etc - all forms of traditional and non-traditional art. Meeting once a week, we do term-length/half-term-length projects based on a concept or theme that is suggested and selected by members, and work on our pieces either individually or collectively. We also hold fun special events at least once a term: guest artist speakers, museum trips, movie screenings or instructional workshops. We aim to occasionally have some of our artwork displayed in public or published in student magazines.", "Open to anyone interested in art, we are a non-competitive, low-pressure, no-experience-necessary club aimed especially, but not exclusively, at recreational artists. Work at your own pace and make whatever you want - art for art's sake, and fun for fun's sake!"],
      src: 'images/year book photo.jpg'
    };

    AboutPageView.prototype.getTemplateData = function() {
      return this.templateData;
    };

    return AboutPageView;

  })(PageView);
  
}});

window.require.define({"views/artist_page_view": function(exports, require, module) {
  var ArtistPageView, PageView, template,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  template = require('views/templates/artist');

  PageView = require('views/base/page_view');

  module.exports = ArtistPageView = (function(_super) {

    __extends(ArtistPageView, _super);

    function ArtistPageView() {
      return ArtistPageView.__super__.constructor.apply(this, arguments);
    }

    ArtistPageView.prototype.template = template;

    ArtistPageView.prototype.className = 'artists-page';

    ArtistPageView.prototype.events = {
      'mouseenter .artist-name': function(e) {
        var $el, $img, i, img, index, _i, _len, _ref, _results;
        $el = $(e.currentTarget);
        index = $el.data('index');
        _ref = this.$('.artist-photo');
        _results = [];
        for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
          img = _ref[i];
          $img = $(img);
          if (index === i) {
            _results.push($img.addClass('active'));
          } else {
            _results.push($img.removeClass('active'));
          }
        }
        return _results;
      }
    };

    ArtistPageView.prototype.templateData = {
      artists: [
        {
          name: 'Cristal Alba',
          photo: 'images/Artists/Cristal Alba/2'
        }, {
          name: 'Jillian Collins',
          photo: 'images/Artists/Jillian Collins/1'
        }, {
          name: 'Stephanie Dunn',
          photo: 'images/Artists/Stephanie Dunn/4'
        }, {
          name: 'Vanessa Goh',
          photo: 'images/Artists/Vanessa Goh/3'
        }, {
          name: 'Victoria Groysberg',
          photo: 'images/Artists/Victoria Groysberg/6'
        }, {
          name: 'Laura Hughes',
          photo: 'images/Artists/Laura Hughes/1'
        }, {
          name: 'Kelly Leow',
          photo: 'images/Artists/Kelly Leow/1'
        }, {
          name: 'Brooke Wities',
          photo: 'images/Artists/Brooke Wities/6'
        }, {
          name: 'Ying Zhi Lim',
          photo: 'images/Artists/Ying Zhi Lim/1'
        }
      ]
    };

    ArtistPageView.prototype.getTemplateData = function() {
      var artist, artists, _i, _len;
      artists = this.templateData.artists;
      artists[0].active = true;
      for (_i = 0, _len = artists.length; _i < _len; _i++) {
        artist = artists[_i];
        artist.href = artist.name.toLowerCase().split(' ').join('_');
      }
      return this.templateData;
    };

    ArtistPageView.prototype.render = function() {
      var $el, $names, $photos, el, i, list, _i, _j, _len, _len1, _ref;
      ArtistPageView.__super__.render.apply(this, arguments);
      $names = this.$('.artist-name');
      $photos = this.$('.artist-photos img');
      _ref = [$names, $photos];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        list = _ref[_i];
        for (i = _j = 0, _len1 = list.length; _j < _len1; i = ++_j) {
          el = list[i];
          $el = $(el);
          $el.data('index', i);
        }
      }
      return this.delegateEvents;
    };

    return ArtistPageView;

  })(PageView);
  
}});

window.require.define({"views/base/collection_view": function(exports, require, module) {
  var Chaplin, CollectionView, View,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Chaplin = require('chaplin');

  View = require('views/base/view');

  module.exports = CollectionView = (function(_super) {

    __extends(CollectionView, _super);

    function CollectionView() {
      return CollectionView.__super__.constructor.apply(this, arguments);
    }

    CollectionView.prototype.getTemplateFunction = View.prototype.getTemplateFunction;

    return CollectionView;

  })(Chaplin.CollectionView);
  
}});

window.require.define({"views/base/page_view": function(exports, require, module) {
  var PageView, View, mediator,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  mediator = require('mediator');

  View = require('views/base/view');

  module.exports = PageView = (function(_super) {

    __extends(PageView, _super);

    function PageView() {
      return PageView.__super__.constructor.apply(this, arguments);
    }

    PageView.prototype.container = '#page-container';

    PageView.prototype.autoRender = true;

    PageView.prototype.renderedSubviews = false;

    PageView.prototype.subviews = [];

    PageView.prototype.initialize = function() {
      var rendered,
        _this = this;
      PageView.__super__.initialize.apply(this, arguments);
      if (this.model || this.collection) {
        rendered = false;
        return this.modelBind('change', function() {
          if (!rendered) {
            _this.render();
          }
          return rendered = true;
        });
      }
    };

    PageView.prototype.renderSubviews = function() {};

    PageView.prototype.render = function() {
      PageView.__super__.render.apply(this, arguments);
      if (!this.renderedSubviews) {
        this.renderSubviews();
        return this.renderedSubviews = true;
      }
    };

    return PageView;

  })(View);
  
}});

window.require.define({"views/base/view": function(exports, require, module) {
  var Chaplin, View,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Chaplin = require('chaplin');

  require('lib/view_helper');

  module.exports = View = (function(_super) {

    __extends(View, _super);

    function View() {
      return View.__super__.constructor.apply(this, arguments);
    }

    View.prototype.getTemplateFunction = function() {
      return this.template;
    };

    return View;

  })(Chaplin.View);
  
}});

window.require.define({"views/calendar_page_view": function(exports, require, module) {
  var CalendarPageView, PageView, template,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  template = require('views/templates/calendar');

  PageView = require('views/base/page_view');

  module.exports = CalendarPageView = (function(_super) {

    __extends(CalendarPageView, _super);

    function CalendarPageView() {
      return CalendarPageView.__super__.constructor.apply(this, arguments);
    }

    CalendarPageView.prototype.template = template;

    CalendarPageView.prototype.className = 'calendar-page';

    CalendarPageView.prototype.templateData = {};

    CalendarPageView.prototype.getTemplateData = function() {
      return this.templateData;
    };

    return CalendarPageView;

  })(PageView);
  
}});

window.require.define({"views/contact_page_view": function(exports, require, module) {
  var ContactPageView, PageView, template,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  template = require('views/templates/contact');

  PageView = require('views/base/page_view');

  module.exports = ContactPageView = (function(_super) {

    __extends(ContactPageView, _super);

    function ContactPageView() {
      return ContactPageView.__super__.constructor.apply(this, arguments);
    }

    ContactPageView.prototype.template = template;

    ContactPageView.prototype.className = 'contacts-page';

    ContactPageView.prototype.templateData = {
      officers: [
        {
          position: 'President',
          info: [
            {
              name: "Vanessa Goh",
              email: 'vldgoh@ucla.edu'
            }
          ]
        }, {
          position: 'Vice President',
          info: [
            {
              name: 'Jessica Lee',
              email: 'jesslee3@ucla.edu'
            }
          ]
        }, {
          position: 'Treasurer',
          info: [
            {
              name: 'Brooke Wities',
              email: 'bwities@gmail.com'
            }
          ]
        }, {
          position: 'Treasurer Committee',
          info: [
            {
              name: 'Emily Chuang',
              email: 'emilykchuang@gmail.com'
            }, {
              name: 'Divya Narasimhan',
              email: 'dnarasimhan29@gmail.com'
            }
          ]
        }, {
          position: 'External',
          info: [
            {
              name: 'Kelly Leow',
              email: 'kellyawesomeleow@gmail.com'
            }
          ]
        }, {
          position: 'External Committee',
          info: [
            {
              name: 'Divya Narasimhan',
              email: 'dnarasimhan29@gmail.com'
            }
          ]
        }, {
          position: 'Web Management',
          info: [
            {
              name: "Devin Abbott",
              email: "devinaabbott@gmail.com"
            }, {
              name: "Cristal Alba",
              email: "cristalgalba@gmail.com"
            }
          ]
        }
      ]
    };

    ContactPageView.prototype.getTemplateData = function() {
      return this.templateData;
    };

    return ContactPageView;

  })(PageView);
  
}});

window.require.define({"views/header_view": function(exports, require, module) {
  var HeaderView, View, template,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  template = require('views/templates/header');

  View = require('views/base/view');

  module.exports = HeaderView = (function(_super) {

    __extends(HeaderView, _super);

    function HeaderView() {
      return HeaderView.__super__.constructor.apply(this, arguments);
    }

    HeaderView.prototype.template = template;

    HeaderView.prototype.el = '#header-container';

    HeaderView.prototype.events = function() {
      return {
        'click h1': function(e) {
          return Backbone.history.navigate('/home', {
            trigger: true
          });
        }
      };
    };

    HeaderView.prototype.getTemplateData = function() {
      return {
        links: [
          {
            title: 'home'
          }, {
            title: 'artists'
          }, {
            title: 'about'
          }, {
            title: 'calendar'
          }, {
            title: 'location'
          }, {
            title: 'contact'
          }
        ]
      };
    };

    return HeaderView;

  })(View);
  
}});

window.require.define({"views/home_page_view": function(exports, require, module) {
  var HomePageView, PageView, template,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  template = require('views/templates/home');

  PageView = require('views/base/page_view');

  module.exports = HomePageView = (function(_super) {

    __extends(HomePageView, _super);

    function HomePageView() {
      return HomePageView.__super__.constructor.apply(this, arguments);
    }

    HomePageView.prototype.template = template;

    HomePageView.prototype.className = 'home-page';

    HomePageView.prototype.events = function() {
      return {
        'click img': function(e) {
          var url;
          url = '/' + ($(e.currentTarget)).data('href');
          return Backbone.history.navigate(url, {
            trigger: true
          });
        }
      };
    };

    HomePageView.prototype.initialize = function() {
      return this.on('addedToDOM', function() {
        $('#featured').orbit();
        $('#featured').css('height', '450px');
        return $('#featured').css('width', '940px');
      });
    };

    HomePageView.prototype.getTemplateData = function() {
      return {
        images: [
          {
            src: 'homepage_images/homepage_brooke',
            href: 'artist/brooke_wities'
          }, {
            src: 'homepage_images/homepage_vanessa',
            href: 'artist/vanessa_goh'
          }, {
            src: 'homepage_images/homepage_jessica',
            href: 'artist/jessica_lee'
          }, {
            src: 'homepage_images/homepage_ying',
            href: 'artist/ying_zhi_lim'
          }, {
            src: 'homepage_images/homepage_laura',
            href: 'artist/laura_hughes'
          }, {
            src: 'homepage_images/homepage_jillian',
            href: 'artist/jillian_collins'
          }
        ]
      };
    };

    return HomePageView;

  })(PageView);
  
}});

window.require.define({"views/individual_page_view": function(exports, require, module) {
  var IndividualPageView, PageView, template,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  template = require('views/templates/individual');

  PageView = require('views/base/page_view');

  module.exports = IndividualPageView = (function(_super) {

    __extends(IndividualPageView, _super);

    IndividualPageView.prototype.template = template;

    IndividualPageView.prototype.className = 'individual-page';

    function IndividualPageView(person, options) {
      this.person = person;
      if (options == null) {
        options = {};
      }
      IndividualPageView.__super__.constructor.call(this, options);
    }

    IndividualPageView.prototype.events = {
      'mouseenter .thumbnail': function(e) {
        var $el, $img, i, img, index, _i, _len, _ref, _results;
        $el = $(e.currentTarget);
        index = $el.index();
        console.log(index);
        _ref = this.$('.images .image');
        _results = [];
        for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
          img = _ref[i];
          console.log(img, i);
          $img = $(img);
          if (index === i) {
            _results.push($img.addClass('active'));
          } else {
            _results.push($img.removeClass('active'));
          }
        }
        return _results;
      }
    };

    IndividualPageView.prototype.templateData = {
      cristal_alba: {
        name: "Cristal Alba",
        bio: "Art is composed of tumultuous instances of mental escapades. Moments caught in the fluid of a pen or the splatter of a brush.             Moments that contain my insanity.",
        images: [
          {
            title: "Alice in Cunterland",
            src: "images/Artists/Cristal Alba/1",
            caption: ""
          }, {
            title: "Embrace the Daze",
            src: "images/Artists/Cristal Alba/2",
            caption: ""
          }, {
            title: "Hardheaded",
            src: "images/Artists/Cristal Alba/3",
            caption: ""
          }, {
            title: "Tragos de Tequila",
            src: "images/Artists/Cristal Alba/4",
            caption: ""
          }, {
            title: "Turbulence",
            src: "images/Artists/Cristal Alba/5",
            caption: ""
          }
        ]
      },
      jillian_collins: {
        name: "Jillian Collins",
        bio: "My name is Jillian Collins. I believe creativity stops not at the canvas, but never stops at all.          I grew up in the Northern California, City of San Carlos. Its 35 minutes from San Francisco, and a few towns over from Palo Alto. It is also kinda boring. So now I think of myself as an LA girl-minus the fake tan.  	In High School I participated in my IB art Exhibition, where I showcased 12 pieces of art I picked from over 2 years. Mostly oils, acrylic, collage, and, um, my wisdom teeth. That was a hit. 	I started at UCLA in 2009, and I Major in History. I met our founders, same year,  in various classes and by forgetting their names. Eventually ASB was born. I work with many materials-ink, acrylic, modge-podge, collage, things I find around my place or elsewhere, pens, pencils, paper, etc. I recently joined the 21st century and got some kind of tablet thing where I can draw on my computer. I also do nail art, write, and expand my artistic horizons often. I believe that you can apply creativity not just to a canvas, but to every obstacle you may face in your life. 	My art, as cliche as it sounds, comes from my soul. I am very open about my obstacles and try to channel them into art. My art is generally some type of reflection of myself, but I do try to tap into the other parts that are just flights of fancy. 	My non-art activities include watching obscene amounts of television, reading detective and horror novels, listening to music (David Bowie is my favorite), being a pop culture junkie, and cracking jokes. I talk a lot, mostly nonsense and history. The people who inspire me are mostly flawed artists/people of note. My favorite artists are Dali, Lichtensenstein, and John Singer Sargent (he painted a portrait of my Grandmother). I love to make people laugh, even if my humor is very much a cross section of Dorothy Parker and Rodney Dangerfield.	Art Shaped Box is a family-a really good one-that will embrace you with open arms. And I promise I'll be the first to say hello!",
        images: [
          {
            title: "",
            src: "images/Artists/Jillian Collins/1",
            caption: ""
          }, {
            title: "",
            src: "images/Artists/Jillian Collins/2",
            caption: ""
          }, {
            title: "",
            src: "images/Artists/Jillian Collins/3",
            caption: ""
          }, {
            title: "",
            src: "images/Artists/Jillian Collins/4",
            caption: ""
          }, {
            title: "Nail Tryptic",
            src: "images/Artists/Jillian Collins/5",
            caption: ""
          }, {
            title: "Serpent e Ocelot",
            src: "images/Artists/Jillian Collins/6",
            caption: ""
          }, {
            title: "Stargirl",
            src: "images/Artists/Jillian Collins/7",
            caption: ""
          }
        ]
      },
      stephanie_dunn: {
        name: "Stephanie Dunn",
        bio: "Stephanie Dunn is a third year UCLA student studying psychobiology, but her interests expand far beyond a biology book.  Her passion for understanding the body and mind is evident in her art, which is often inspired by mental illness and the people who struggle without a voice due to the stigma surrounding their illnesses.  Another major theme Stephanie explores is sexuality, especially alternative sexualities and women expression of individuality, unique sexualities.  Using the female form, Stephanie strives to spread awareness of the women and LGBT issues that inspire her artwork.  Fascinated by all forms of art, including tattoos (which often appear in her work) and poetry, Stephanie uses graphite, watercolor, ink, colored pencil, and other mediums to glorify the body and explore the mind.",
        images: [
          {
            title: "",
            src: "images/Artists/Stephanie Dunn/1",
            caption: ""
          }, {
            title: "",
            src: "images/Artists/Stephanie Dunn/2",
            caption: ""
          }, {
            title: "",
            src: "images/Artists/Stephanie Dunn/3",
            caption: ""
          }, {
            title: "",
            src: "images/Artists/Stephanie Dunn/4",
            caption: ""
          }, {
            title: "",
            src: "images/Artists/Stephanie Dunn/5",
            caption: ""
          }, {
            title: "",
            src: "images/Artists/Stephanie Dunn/6",
            caption: ""
          }
        ]
      },
      vanessa_goh: {
        name: "Vanessa Goh",
        bio: "I create in order to reveal something about myself that I never knew... to awaken thoughts and feelings hidden away in my heart. My only wish is that whoever sees my creations can discover the beauty or the horror inside their own soul and aspire to imagine and create a new world for themselves.            That and I like to draw sexy women. And Batman. He is cool too.",
        images: [
          {
            title: "",
            src: "images/Artists/Vanessa Goh/1",
            caption: ""
          }, {
            title: "Deadpool",
            src: "images/Artists/Vanessa Goh/2",
            caption: ""
          }, {
            title: "Give Me My Right to Love",
            src: "images/Artists/Vanessa Goh/3",
            caption: ""
          }, {
            title: "Phoenix Black Dev",
            src: "images/Artists/Vanessa Goh/4",
            caption: ""
          }
        ]
      },
      victoria_groysberg: {
        name: "Victoria Groysberg",
        bio: "Victoria Groysberg is a Cognitive Science major at UCLA.  Victoria uses vibrant color in her work to reflect the brightness and humor in which she sees the world in. She focuses on human emotion and tries to capture the essence of beautiful moments. Impressionists,  who are able to give motion and life to static objects, greatly influence her. Victoria has recently discovered a new art form, dance, which she pursues by taking dance courses of different styles at UCLA.   Victoria has participated in two exhibitions at UCLA, \"Asian Minority\" and \"In Concert With Nature,\" and wishes to participate in more exhibitions.  She wants to give back to UCLA, to her community, and to the world, through art and medicine.  She aims for an MD/PhD.",
        images: [
          {
            title: "A Little Hungry",
            src: "images/Artists/Victoria Groysberg/1"
          }, {
            title: "Copy of Kuinji",
            src: "images/Artists/Victoria Groysberg/2"
          }, {
            title: "Copy of Manet's Girl at the Bar",
            src: "images/Artists/Victoria Groysberg/3"
          }, {
            title: "Copy of Shishkin",
            src: "images/Artists/Victoria Groysberg/4"
          }, {
            title: "Lots of Grapes",
            src: "images/Artists/Victoria Groysberg/5"
          }, {
            title: "Sea and Boats",
            src: "images/Artists/Victoria Groysberg/6"
          }, {
            title: "View from the Window",
            src: "images/Artists/Victoria Groysberg/7"
          }
        ]
      },
      laura_hughes: {
        name: "Laura Hughes",
        bio: "My name is Laura Hughes, I am an Applied Mathematics Major, and I am crazy about mixed medium art.  I like acrylic and oil paints, graphite, pen and collage in that order.  My art always has to have something alive in it, and I especially love learning new techniques and trying them out on Artist Trading Cards.  Always traded, never sold!",
        images: [
          {
            title: "",
            src: "images/Artists/Laura Hughes/1",
            caption: ""
          }, {
            title: "",
            src: "images/Artists/Laura Hughes/2",
            caption: ""
          }
        ]
      },
      kelly_leow: {
        name: "Kelly Leow",
        bio: "Kelly grew up in Singapore. When she was born, a celebrated Indian astrologer read in the stars that she would someday achieve great fame, but the stars did not specify for what; her parents live in fear of the worst (porn actress, serial killer). Her scholarly interests include Franz Kafka, Jackson Pollock, Twin Peaks, Bear Grylls, the Back to the Future franchise, sheep cheese, HBO's Girls, Stephen Fry, The National, Mad Men, and Trader Joe's Cookie Butter.",
        images: [
          {
            title: "",
            src: "images/Artists/Kelly Leow/1"
          }, {
            title: "",
            src: "images/Artists/Kelly Leow/2"
          }, {
            title: "",
            src: "images/Artists/Kelly Leow/3"
          }, {
            title: "",
            src: "images/Artists/Kelly Leow/4"
          }, {
            title: "",
            src: "images/Artists/Kelly Leow/5"
          }, {
            title: "",
            src: "images/Artists/Kelly Leow/6"
          }
        ]
      },
      brooke_wities: {
        name: "Brooke Wities",
        bio: "Brooke was born with out the ability to speak. After about a year, she conquered that challenge and moved on to her next battle: learning to draw.  Brooke was fortunate enough to have been raised amongst the high quality artistic medium, crayola crayons, at a young age.  Her artistic endeavors and exploration of media progressed, as she grew older.  She enrolled in a wide range of art classes at her public high school, which encouraged art and creativity.  She continued onto UCLA where she co-founded the student organization Art Shaped Box and is currently majoring in Nursing.            Brooke often found herself paying absolutely no attention in her nursing lectures to the verbal content, but was very focused on the anatomical images. She frequently spends her time in lecture doodling brains, human forms, skeletons, and various organs.  This has played a major role in her artwork, which tends to be anatomically influenced and surreal.            Friends and family of Brooke think her artwork is very good.  That is saying something since Brooke is surrounded by many critical people.  However, many notable and influential people have never seen her artwork.  Once Christian bale stood 20 feet away from many of her paintings and even almost saw them. He was almost impressed by them.",
        images: [
          {
            title: "",
            src: "images/Artists/Brooke Wities/1",
            caption: ""
          }, {
            title: "",
            src: "images/Artists/Brooke Wities/2",
            caption: ""
          }, {
            title: "",
            src: "images/Artists/Brooke Wities/3",
            caption: ""
          }, {
            title: "",
            src: "images/Artists/Brooke Wities/4",
            caption: ""
          }, {
            title: "",
            src: "images/Artists/Brooke Wities/5",
            caption: ""
          }, {
            title: "",
            src: "images/Artists/Brooke Wities/6",
            caption: ""
          }, {
            title: "",
            src: "images/Artists/Brooke Wities/7",
            caption: ""
          }, {
            title: "",
            src: "images/Artists/Brooke Wities/8",
            caption: ""
          }
        ]
      },
      ying_zhi_lim: {
        name: "Ying Zhi Lim",
        bio: "I do a large variety of art, from painting to photography to crafting, I'd like to think myself as a jack of all trades, utilizing my various skills in many fields. I've sold my works on stores, creating a business out of them while still using art as an outlet for artistic expression.",
        images: [
          {
            title: "",
            src: "images/Artists/Ying Zhi Lim/1",
            caption: ""
          }, {
            title: "",
            src: "images/Artists/Ying Zhi Lim/2",
            caption: ""
          }, {
            title: "",
            src: "images/Artists/Ying Zhi Lim/3",
            caption: ""
          }, {
            title: "",
            src: "images/Artists/Ying Zhi Lim/4",
            caption: ""
          }
        ]
      }
    };

    IndividualPageView.prototype.getTemplateData = function() {
      var data;
      data = this.templateData[this.person];
      if (data.images[0] != null) {
        data.images[0].active = true;
      }
      return data;
    };

    return IndividualPageView;

  })(PageView);
  
}});

window.require.define({"views/layout": function(exports, require, module) {
  var Chaplin, Layout,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Chaplin = require('chaplin');

  module.exports = Layout = (function(_super) {

    __extends(Layout, _super);

    function Layout() {
      return Layout.__super__.constructor.apply(this, arguments);
    }

    Layout.prototype.initialize = function() {
      return Layout.__super__.initialize.apply(this, arguments);
    };

    return Layout;

  })(Chaplin.Layout);
  
}});

window.require.define({"views/location_page_view": function(exports, require, module) {
  var LocationPageView, PageView, template,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  template = require('views/templates/location');

  PageView = require('views/base/page_view');

  module.exports = LocationPageView = (function(_super) {

    __extends(LocationPageView, _super);

    function LocationPageView() {
      return LocationPageView.__super__.constructor.apply(this, arguments);
    }

    LocationPageView.prototype.template = template;

    LocationPageView.prototype.className = 'location-page';

    LocationPageView.prototype.templateData = {
      text: "We're located in the Mesa room of the sunset rec center.",
      loading: "Loading google map of our location..."
    };

    LocationPageView.prototype.getTemplateData = function() {
      return this.templateData;
    };

    LocationPageView.prototype.render = function() {
      var _this = this;
      LocationPageView.__super__.render.apply(this, arguments);
      return this.on('addedToDOM', function() {
        return $("iframe").load(function() {
          console.log('IFRAME LOADED');
          return _this.$('.loading').addClass('hide');
        });
      });
    };

    return LocationPageView;

  })(PageView);
  
}});

window.require.define({"views/templates/about": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", stack1, stack2, foundHelper, tmp1, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;

  function program1(depth0,data) {
    
    var buffer = "", stack1;
    buffer += "\n  <div class=\"info\">";
    stack1 = depth0;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "this", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</div>\n  ";
    return buffer;}

    buffer += "<div class=\"about\">\n  ";
    foundHelper = helpers.info;
    stack1 = foundHelper || depth0.info;
    stack2 = helpers.each;
    tmp1 = self.program(1, program1, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n  <img class=\"image\" src=\"";
    foundHelper = helpers.src_prefix;
    stack1 = foundHelper || depth0.src_prefix;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "src_prefix", { hash: {} }); }
    buffer += escapeExpression(stack1);
    foundHelper = helpers.src;
    stack1 = foundHelper || depth0.src;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "src", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\"></img>\n</div>";
    return buffer;});
}});

window.require.define({"views/templates/artist": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", stack1, stack2, foundHelper, tmp1, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;

  function program1(depth0,data) {
    
    var buffer = "", stack1;
    buffer += "\n      <a class=\"artist-name\" href=\"/artist/";
    foundHelper = helpers.href;
    stack1 = foundHelper || depth0.href;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "href", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\">";
    foundHelper = helpers.name;
    stack1 = foundHelper || depth0.name;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "name", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</a>\n    ";
    return buffer;}

  function program3(depth0,data) {
    
    var buffer = "", stack1, stack2;
    buffer += "\n      <img class=\"artist-photo ";
    foundHelper = helpers.active;
    stack1 = foundHelper || depth0.active;
    stack2 = helpers['if'];
    tmp1 = self.program(4, program4, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\" src=\"";
    foundHelper = helpers.src_prefix;
    stack1 = foundHelper || depth0.src_prefix;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "src_prefix", { hash: {} }); }
    buffer += escapeExpression(stack1);
    foundHelper = helpers.photo;
    stack1 = foundHelper || depth0.photo;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "photo", { hash: {} }); }
    buffer += escapeExpression(stack1) + "_m.jpg\"></img>\n    ";
    return buffer;}
  function program4(depth0,data) {
    
    
    return "active";}

    buffer += "<div class=\"artists-page\">\n  <div class=\"artist-names\">\n    ";
    foundHelper = helpers.artists;
    stack1 = foundHelper || depth0.artists;
    stack2 = helpers.each;
    tmp1 = self.program(1, program1, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n  </div>\n  <div class=\"artist-photos\">\n    ";
    foundHelper = helpers.artists;
    stack1 = foundHelper || depth0.artists;
    stack2 = helpers.each;
    tmp1 = self.program(3, program3, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n  </div>\n</div>\n";
    return buffer;});
}});

window.require.define({"views/templates/calendar": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var foundHelper, self=this;


    return "<iframe src=\"https://www.google.com/calendar/embed?src=artshapedbox.ucla%40gmail.com&ctz=America/Los_Angeles\" style=\"border: 0\" width=\"800\" height=\"600\" frameborder=\"0\" scrolling=\"no\"></iframe>";});
}});

window.require.define({"views/templates/contact": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", stack1, stack2, foundHelper, tmp1, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;

  function program1(depth0,data) {
    
    var buffer = "", stack1, stack2;
    buffer += "\n    <div class=\"officer-position\">\n      <div>";
    foundHelper = helpers.position;
    stack1 = foundHelper || depth0.position;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "position", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</div>\n      ";
    foundHelper = helpers.info;
    stack1 = foundHelper || depth0.info;
    stack2 = helpers.each;
    tmp1 = self.program(2, program2, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n    </div>\n    ";
    return buffer;}
  function program2(depth0,data) {
    
    var buffer = "", stack1;
    buffer += "\n      <div class=\"officer-info\">\n        <span>";
    foundHelper = helpers.name;
    stack1 = foundHelper || depth0.name;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "name", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</span>\n        <a href=\"mailto:";
    foundHelper = helpers.email;
    stack1 = foundHelper || depth0.email;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "email", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\" class=\"officer-email\">";
    foundHelper = helpers.email;
    stack1 = foundHelper || depth0.email;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "email", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</a>\n      </div>\n      ";
    return buffer;}

    buffer += "<div class=\"contacts-page\">\n  <div class=\"officers\">\n    ";
    foundHelper = helpers.officers;
    stack1 = foundHelper || depth0.officers;
    stack2 = helpers.each;
    tmp1 = self.program(1, program1, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n  </div>\n  </div>";
    return buffer;});
}});

window.require.define({"views/templates/header": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", stack1, stack2, foundHelper, tmp1, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;

  function program1(depth0,data) {
    
    var buffer = "", stack1;
    buffer += "\n  <a href=\"/";
    foundHelper = helpers.title;
    stack1 = foundHelper || depth0.title;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "title", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\">";
    foundHelper = helpers.title;
    stack1 = foundHelper || depth0.title;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "title", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</a>\n";
    return buffer;}

    buffer += "<h1>Art Shaped Box</h1>\n";
    foundHelper = helpers.links;
    stack1 = foundHelper || depth0.links;
    stack2 = helpers.each;
    tmp1 = self.program(1, program1, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    return buffer;});
}});

window.require.define({"views/templates/home": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", stack1, stack2, foundHelper, tmp1, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;

  function program1(depth0,data) {
    
    var buffer = "", stack1;
    buffer += "\n  <img src=\"";
    foundHelper = helpers.src_prefix;
    stack1 = foundHelper || depth0.src_prefix;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "src_prefix", { hash: {} }); }
    buffer += escapeExpression(stack1);
    foundHelper = helpers.src;
    stack1 = foundHelper || depth0.src;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "src", { hash: {} }); }
    buffer += escapeExpression(stack1) + ".png\" data-href=\"";
    foundHelper = helpers.href;
    stack1 = foundHelper || depth0.href;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "href", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\" />\n  ";
    return buffer;}

    buffer += "<div id=\"featured\"> \n  ";
    foundHelper = helpers.images;
    stack1 = foundHelper || depth0.images;
    stack2 = helpers.each;
    tmp1 = self.program(1, program1, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n</div>";
    return buffer;});
}});

window.require.define({"views/templates/individual": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", stack1, stack2, foundHelper, tmp1, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;

  function program1(depth0,data) {
    
    var buffer = "", stack1;
    buffer += " \n    <img class=\"thumbnail\" src=\"";
    foundHelper = helpers.src_prefix;
    stack1 = foundHelper || depth0.src_prefix;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "src_prefix", { hash: {} }); }
    buffer += escapeExpression(stack1);
    foundHelper = helpers.src;
    stack1 = foundHelper || depth0.src;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "src", { hash: {} }); }
    buffer += escapeExpression(stack1) + "_s.jpg\"></img>\n  ";
    return buffer;}

  function program3(depth0,data) {
    
    var buffer = "", stack1, stack2;
    buffer += "\n    <div class=\"image ";
    foundHelper = helpers.active;
    stack1 = foundHelper || depth0.active;
    stack2 = helpers['if'];
    tmp1 = self.program(4, program4, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\">\n      <div class=\"title\">";
    foundHelper = helpers.title;
    stack1 = foundHelper || depth0.title;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "title", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</div>\n      <img src=\"";
    foundHelper = helpers.src_prefix;
    stack1 = foundHelper || depth0.src_prefix;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "src_prefix", { hash: {} }); }
    buffer += escapeExpression(stack1);
    foundHelper = helpers.src;
    stack1 = foundHelper || depth0.src;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "src", { hash: {} }); }
    buffer += escapeExpression(stack1) + "_m.jpg\"></img>\n      <!--<div class=\"caption\">";
    foundHelper = helpers.caption;
    stack1 = foundHelper || depth0.caption;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "caption", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</div>-->\n    </div>\n    ";
    return buffer;}
  function program4(depth0,data) {
    
    
    return "active";}

    buffer += "<!--\nNOTE: There is actually a <div class=\"individual-page\"> around this page\nwhich you can use in the CSS\n\nAlso, in Komodo, at the bottom right, you can set 'Text' to 'HTML5'\nand it will highlight this page for you.\n!-->\n\n<div class=\"name\">";
    foundHelper = helpers.name;
    stack1 = foundHelper || depth0.name;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "name", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</div>\n<div class=\"bio\">";
    foundHelper = helpers.bio;
    stack1 = foundHelper || depth0.bio;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "bio", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</div>\n<div class=\"image-area\">\n  <div class=\"thumbnails\">\n    ";
    foundHelper = helpers.images;
    stack1 = foundHelper || depth0.images;
    stack2 = helpers.each;
    tmp1 = self.program(1, program1, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "  \n  </div>\n  <div class=\"images\">\n    ";
    foundHelper = helpers.images;
    stack1 = foundHelper || depth0.images;
    stack2 = helpers.each;
    tmp1 = self.program(3, program3, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n  </div>\n</div>";
    return buffer;});
}});

window.require.define({"views/templates/location": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", stack1, foundHelper, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;


    buffer += "<div class=\"test\">";
    foundHelper = helpers.text;
    stack1 = foundHelper || depth0.text;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "text", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</div>\n<div class=\"loading\">";
    foundHelper = helpers.loading;
    stack1 = foundHelper || depth0.loading;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "loading", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</div>\n<iframe id=\"map\" width=\"680\" height=\"560\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"https://maps.google.com/maps/ms?msa=0&amp;msid=211050225043110450746.0004c983cebbcc8792f2a&amp;ie=UTF8&amp;ll=34.075593,-118.452278&amp;spn=0,0&amp;t=h&amp;output=embed\"></iframe>\n";
    return buffer;});
}});

