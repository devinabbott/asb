#!/bin/bash

#DIR="app/assets/images/Artists/Brooke Wities/"
#DIR="app/assets/images/Artists/Jillian Collins/"
#DIR="app/assets/images/Artists/Cristal Alba/"
#DIR="app/assets/images/Artists/Jessica Lee/"
#DIR="app/assets/images/Artists/Laura Hughes/"
#DIR="app/assets/images/Artists/Stephanie Dunn/"
#DIR="app/assets/images/Artists/Vanessa Goh/"
#DIR="app/assets/images/Artists/Ying Zhi Lim/"
DIR="app/assets/images/Artists/Victoria Groysberg/"
#INPATH="app/assets/images/Artists/Brooke Wities/1.jpg"


IFS=$'\n'

#DIR="app/assets/images/Artists/Jillian Collins/"

let i=1
for FILE in `find "${DIR}" -maxdepth 1 -mindepth 1 -type f -exec basename '{}' \;`
do
  FILEN=${FILE}
  FILEN=${FILEN%.JPG}
  FILEN=${FILEN%.JPEG}
  FILEN=${FILEN%.jpeg}
  FILEN=${FILEN%.jpg}
  #i=`expr "${i}+1"`
  #F="${DIR}/${FILE}"
  echo ${FILEN}
  echo ${i}
  mv "${DIR}/${FILE}" "${DIR}/${i}.jpg"
  
  let i=i+1
done

MAXWIDTH=720
MAXHEIGHT=700

for FILE in `find "${DIR}" -maxdepth 1 -mindepth 1 -type f -exec basename '{}' \;`
do
  F="${DIR}/${FILE}"
  DIMS=`identify "${F}" | awk '{print $4}'`
  WIDTH=`echo "${DIMS}"| cut -dx -f1`
  HEIGHT=`echo "${DIMS}"| cut -dx -f2`
  
  if [[ ${WIDTH} > ${HEIGHT} ]]
  then
      echo "(Landscape orientation) "
      #RATIO="${WIDTH} / ${MAXWIDTH}"
      #R=`echo ${RATIO} | bc -l`
      M=720
      S=180
  else
      #file is in portrait orientation
      echo "(Portrait orientation) "
      #RATIO="${HEIGHT} / ${MAXHEIGHT}"
      #R=`echo ${RATIO} | bc -l`
      M=700
      S=180
  fi
  #NWS="${WIDTH} / ${R}"
  #NW=`echo ${NWS} | bc -l`
  #NWF=`printf %0.f ${NW}`
  #
  #NHS="${HEIGHT} / ${R}"
  #NH=`echo ${NHS} | bc -l`
  #NHF=`printf %0.f ${NH}`
  #
  #MED="${NWF}x${NHF}"
  #echo ${MED}
  #echo 700/${}
  
  FILEN=${FILE%.jpg}
  #echo ${FILEN}
  
  convert "${F}" -resize "${M}x${M}" "${DIR}/${FILEN}_m.jpg"
  convert "${F}" -resize "${S}x${S}" "${DIR}/${FILEN}_s.jpg"

done