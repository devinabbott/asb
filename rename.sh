#!/bin/bash

IFS=$'\n'

DIR="app/assets/images/Artists/Kelly Leow/"

let i=1
for FILE in `find "${DIR}" -maxdepth 1 -mindepth 1 -type f -exec basename '{}' \;`
do
  FILEN=${FILE}
  FILEN=${FILEN%.JPG}
  FILEN=${FILEN%.JPEG}
  FILEN=${FILEN%.jpeg}
  FILEN=${FILEN%.jpg}
  #i=`expr "${i}+1"`
  #F="${DIR}/${FILE}"
  echo ${FILEN}
  echo ${i}
  mv "${DIR}/${FILE}" "${DIR}/${i}.jpg"
  
  let i=i+1
done