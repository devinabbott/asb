module.exports = (match) ->
  match '', 'home#index'
  match 'about', 'home#about'
  match 'home', 'home#index'
  match 'location', 'home#location'
  match 'calendar', 'home#calendar'
  match 'artist/:name', 'home#individual'
  match 'artists', 'home#artists'
  match 'contact', 'home#contacts'