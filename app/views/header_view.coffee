template = require 'views/templates/header'
View = require 'views/base/view'

module.exports = class HeaderView extends View
  template: template
  el: '#header-container'
  
  events: ->
    'click h1' : ( e ) ->
      Backbone.history.navigate '/home', { trigger: true }
  
  getTemplateData: ->
    {
      links: [ 
        { title: 'home' }
        { title: 'artists' }
        { title: 'about' }
        { title: 'calendar' }
        { title: 'location' }
        { title: 'contact' }
      ]
    }