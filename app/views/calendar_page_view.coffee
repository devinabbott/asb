template = require 'views/templates/calendar'
PageView = require 'views/base/page_view'

module.exports = class CalendarPageView extends PageView
  template: template
  className: 'calendar-page'
  
  templateData: {}
  
  getTemplateData: ->
    @templateData