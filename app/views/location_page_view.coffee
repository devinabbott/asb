template = require 'views/templates/location'
PageView = require 'views/base/page_view'

module.exports = class LocationPageView extends PageView
  template: template
  className: 'location-page'
  
  templateData: {
    text: "We're located in the Mesa room of the sunset rec center."
    loading: "Loading google map of our location..."
  }

  getTemplateData: ->
    @templateData

  render: ->
    super
    @.on 'addedToDOM', =>
      #console.log $ 'iframe'
      $("iframe").load =>
        console.log 'IFRAME LOADED'
        @$('.loading').addClass 'hide'
