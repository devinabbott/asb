template = require 'views/templates/artist'
PageView = require 'views/base/page_view'

module.exports = class ArtistPageView extends PageView
  template: template
  className: 'artists-page'
  
  events: {
    'mouseenter .artist-name' : ( e ) ->
      $el = $ e.currentTarget
      index = $el.data 'index'
      for img, i in @$ '.artist-photo'
        $img = $ img
        if index == i
          $img.addClass 'active'
        else
          $img.removeClass 'active'
  }
  
  templateData: {
    artists: [ 
      {
        name: 'Cristal Alba'
        photo: 'images/Artists/Cristal Alba/2'
      }
      {
        name: 'Jillian Collins'
        photo: 'images/Artists/Jillian Collins/1'
      }
      {
        name: 'Stephanie Dunn'
        photo: 'images/Artists/Stephanie Dunn/4'
      }
      {
        name: 'Vanessa Goh'
        photo: 'images/Artists/Vanessa Goh/3'
      }
      {
        name: 'Victoria Groysberg'
        photo: 'images/Artists/Victoria Groysberg/6'
      }
      { 
        name: 'Laura Hughes'
        photo: 'images/Artists/Laura Hughes/1'
      }
      {
        name: 'Kelly Leow'
        photo: 'images/Artists/Kelly Leow/1'
      }
      { 
        name: 'Brooke Wities'
        photo: 'images/Artists/Brooke Wities/6'
      }
      { 
        name: 'Ying Zhi Lim'
        photo: 'images/Artists/Ying Zhi Lim/1'
      } 
    ]
  }

  getTemplateData: ->
    artists = @templateData.artists
    artists[0].active = true
    for artist in artists
      artist.href = artist.name.toLowerCase().split(' ').join('_')
    return @templateData
    
  render: ->
    super
    $names = @$ '.artist-name'
    $photos = @$ '.artist-photos img'
    for list in [ $names, $photos ]
      for el, i in list
        $el = $ el
        $el.data 'index', i
    @delegateEvents
