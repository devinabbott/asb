template = require 'views/templates/about'
PageView = require 'views/base/page_view'

module.exports = class AboutPageView extends PageView
  template: template
  className: 'about-page'
  
  templateData: {
    info: [
      """Art Shaped Box is a club for UCLA students who want to practice and explore visual art in different mediums, such as painting, illustration, photography, sculpture, digital art, installation art, guerilla art, etc - all forms of traditional and non-traditional art. Meeting once a week, we do term-length/half-term-length projects based on a concept or theme that is suggested and selected by members, and work on our pieces either individually or collectively. We also hold fun special events at least once a term: guest artist speakers, museum trips, movie screenings or instructional workshops. We aim to occasionally have some of our artwork displayed in public or published in student magazines."""
      """Open to anyone interested in art, we are a non-competitive, low-pressure, no-experience-necessary club aimed especially, but not exclusively, at recreational artists. Work at your own pace and make whatever you want - art for art's sake, and fun for fun's sake!"""
    ]
    src: 'images/year book photo.jpg'
  }
  
  getTemplateData: ->
    @templateData