template = require 'views/templates/home'
PageView = require 'views/base/page_view'

module.exports = class HomePageView extends PageView
  template: template
  className: 'home-page'
  
  events: ->
    'click img': ( e ) ->
      url = '/' + ($ e.currentTarget).data 'href'
      Backbone.history.navigate url, { trigger: true }
  
  initialize: ->
    @on 'addedToDOM', ->
      $('#featured').orbit()
      $('#featured').css 'height', '450px'
      $('#featured').css 'width', '940px'
  
  getTemplateData: ->
    {
      images: [
        { src: 'homepage_images/homepage_brooke', href: 'artist/brooke_wities' }
        { src: 'homepage_images/homepage_vanessa', href: 'artist/vanessa_goh' }
        { src: 'homepage_images/homepage_jessica', href: 'artist/jessica_lee' }
        { src: 'homepage_images/homepage_ying', href: 'artist/ying_zhi_lim' }
        { src: 'homepage_images/homepage_laura', href: 'artist/laura_hughes' }
        #{ src: 'homepage_images/homepage_stephanie', href: 'artist/stephanie_dunn' }
        { src: 'homepage_images/homepage_jillian', href: 'artist/jillian_collins' }
      ]
    }