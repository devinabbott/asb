template = require 'views/templates/individual'
PageView = require 'views/base/page_view'

module.exports = class IndividualPageView extends PageView
  template: template
  className: 'individual-page'
  
  constructor: ( @person, options = {} ) ->
    super options
    
  events: {
    'mouseenter .thumbnail' : ( e ) ->
      $el = $ e.currentTarget
      #index = $el.data 'index'
      index = $el.index()
      console.log index
      for img, i in @$ '.images .image'
        console.log img, i
        $img = $ img
        if index == i
          $img.addClass 'active'
        else
          $img.removeClass 'active'
  }
    
  # The way I have this set up:
  #
  # Put data for each person, and only the data within the {
  #    name: "Jillian"
  #    ...
  # }
  # will be sent to the 'individual.hbs' file
  # 
  templateData: {
    cristal_alba: {
      name: "Cristal Alba"
      bio: "Art is composed of tumultuous instances of mental escapades. Moments caught in the fluid of a pen or the splatter of a brush. 
      
      Moments that contain my insanity."
      images: [
        {
          title: "Alice in Cunterland"
          src: "images/Artists/Cristal Alba/1"
          caption: ""
        }
        {
          title: "Embrace the Daze"
          src: "images/Artists/Cristal Alba/2"
          caption: ""
        }
        {
          title: "Hardheaded"
          src: "images/Artists/Cristal Alba/3"
          caption: ""
        }
        {
          title: "Tragos de Tequila"
          src: "images/Artists/Cristal Alba/4"
          caption: ""
        }
        {
          title: "Turbulence"
          src: "images/Artists/Cristal Alba/5"
          caption: ""
        }
      ]
    }
    jillian_collins: {
      name: "Jillian Collins"
      bio: "My name is Jillian Collins. I believe creativity stops not at the canvas, but never stops at all.  
        I grew up in the Northern California, City of San Carlos. Its 35 minutes from San Francisco, and a few towns over from Palo Alto. It is also kinda boring. So now I think of myself as an LA girl-minus the fake tan.  
	In High School I participated in my IB art Exhibition, where I showcased 12 pieces of art I picked from over 2 years. Mostly oils, acrylic, collage, and, um, my wisdom teeth. That was a hit. 
	I started at UCLA in 2009, and I Major in History. I met our founders, same year,  in various classes and by forgetting their names. Eventually ASB was born. I work with many materials-ink, acrylic, modge-podge, collage, things I find around my place or elsewhere, pens, pencils, paper, etc. I recently joined the 21st century and got some kind of tablet thing where I can draw on my computer. I also do nail art, write, and expand my artistic horizons often. I believe that you can apply creativity not just to a canvas, but to every obstacle you may face in your life. 
	My art, as cliche as it sounds, comes from my soul. I am very open about my obstacles and try to channel them into art. My art is generally some type of reflection of myself, but I do try to tap into the other parts that are just flights of fancy. 
	My non-art activities include watching obscene amounts of television, reading detective and horror novels, listening to music (David Bowie is my favorite), being a pop culture junkie, and cracking jokes. I talk a lot, mostly nonsense and history. The people who inspire me are mostly flawed artists/people of note. My favorite artists are Dali, Lichtensenstein, and John Singer Sargent (he painted a portrait of my Grandmother). I love to make people laugh, even if my humor is very much a cross section of Dorothy Parker and Rodney Dangerfield.
	Art Shaped Box is a family-a really good one-that will embrace you with open arms. And I promise I'll be the first to say hello!"
      images: [
        {
          title: ""
          src: "images/Artists/Jillian Collins/1"
          caption: ""
        }
        {
          title: ""
          src: "images/Artists/Jillian Collins/2"
          caption: ""
        }
        {
          title: ""
          src: "images/Artists/Jillian Collins/3"
          caption: ""
        }
        {
          title: ""
          src: "images/Artists/Jillian Collins/4"
          caption: ""
        }
        {
          title: "Nail Tryptic"
          src: "images/Artists/Jillian Collins/5"
          caption: ""
        }
        {
          title: "Serpent e Ocelot"
          src: "images/Artists/Jillian Collins/6"
          caption: ""
        }
        {
          title: "Stargirl"
          src: "images/Artists/Jillian Collins/7"
          caption: ""
        }
      ]
    },
    stephanie_dunn: {
      name: "Stephanie Dunn"
      bio: "Stephanie Dunn is a third year UCLA student studying psychobiology, but her interests expand far beyond a biology book.  Her passion for understanding the body and mind is evident in her art, which is often inspired by mental illness and the people who struggle without a voice due to the stigma surrounding their illnesses.  Another major theme Stephanie explores is sexuality, especially alternative sexualities and women expression of individuality, unique sexualities.  Using the female form, Stephanie strives to spread awareness of the women and LGBT issues that inspire her artwork.  Fascinated by all forms of art, including tattoos (which often appear in her work) and poetry, Stephanie uses graphite, watercolor, ink, colored pencil, and other mediums to glorify the body and explore the mind."
      images: [
        {
          title: ""
          src: "images/Artists/Stephanie Dunn/1"
          caption: ""
        }
        {
          title: ""
          src: "images/Artists/Stephanie Dunn/2"
          caption: ""
        }
        {
          title: ""
          src: "images/Artists/Stephanie Dunn/3"
          caption: ""
        }
        {
          title: ""
          src: "images/Artists/Stephanie Dunn/4"
          caption: ""
        }
        {
          title: ""
          src: "images/Artists/Stephanie Dunn/5"
          caption: ""
        }
        {
          title: ""
          src: "images/Artists/Stephanie Dunn/6"
          caption: ""
        }
      ]
    }
    vanessa_goh: {
      name: "Vanessa Goh"
      bio: "I create in order to reveal something about myself that I never knew... to awaken thoughts and feelings hidden away in my heart. My only wish is that whoever sees my creations can discover the beauty or the horror inside their own soul and aspire to imagine and create a new world for themselves.
            That and I like to draw sexy women. And Batman. He is cool too."
      images: [
        {
          title: ""
          src: "images/Artists/Vanessa Goh/1"
          caption: ""
        }
        {
          title: "Deadpool"
          src: "images/Artists/Vanessa Goh/2"
          caption: ""
        }
        {
          title: "Give Me My Right to Love"
          src: "images/Artists/Vanessa Goh/3"
          caption: ""
        }
        {
          title: "Phoenix Black Dev"
          src: "images/Artists/Vanessa Goh/4"
          caption: ""
        }
      ]
    }
    victoria_groysberg: {
      name: "Victoria Groysberg"
      bio: """Victoria Groysberg is a Cognitive Science major at UCLA.  Victoria uses vibrant color in her work to reflect the brightness and humor in which she sees the world in. She focuses on human emotion and tries to capture the essence of beautiful moments. Impressionists,  who are able to give motion and life to static objects, greatly influence her. Victoria has recently discovered a new art form, dance, which she pursues by taking dance courses of different styles at UCLA.   Victoria has participated in two exhibitions at UCLA, "Asian Minority" and "In Concert With Nature," and wishes to participate in more exhibitions.  She wants to give back to UCLA, to her community, and to the world, through art and medicine.  She aims for an MD/PhD."""
      images: [
        {
          title: "A Little Hungry"
          src: "images/Artists/Victoria Groysberg/1"
        }
        {
          title: "Copy of Kuinji"
          src: "images/Artists/Victoria Groysberg/2"
        }
        {
          title: "Copy of Manet's Girl at the Bar"
          src: "images/Artists/Victoria Groysberg/3"
        }
        {
          title: "Copy of Shishkin"
          src: "images/Artists/Victoria Groysberg/4"
        }
        {
          title: "Lots of Grapes"
          src: "images/Artists/Victoria Groysberg/5"
        }
        {
          title: "Sea and Boats"
          src: "images/Artists/Victoria Groysberg/6"
        }
        {
          title: "View from the Window"
          src: "images/Artists/Victoria Groysberg/7"
        }
      ]
    }
    laura_hughes: {
      name: "Laura Hughes"
      bio: "My name is Laura Hughes, I am an Applied Mathematics Major, and I am crazy about mixed medium art.  I like acrylic and oil paints, graphite, pen and collage in that order.  My art always has to have something alive in it, and I especially love learning new techniques and trying them out on Artist Trading Cards.  Always traded, never sold!"
      images: [
        {
          title: ""
          src: "images/Artists/Laura Hughes/1"
          caption: ""
        }
        {
          title: ""
          src: "images/Artists/Laura Hughes/2"
          caption: ""
        }
      ]
    }
    kelly_leow: {
      name: "Kelly Leow"
      bio: "Kelly grew up in Singapore. When she was born, a celebrated Indian astrologer read in the stars that she would someday achieve great fame, but the stars did not specify for what; her parents live in fear of the worst (porn actress, serial killer). Her scholarly interests include Franz Kafka, Jackson Pollock, Twin Peaks, Bear Grylls, the Back to the Future franchise, sheep cheese, HBO's Girls, Stephen Fry, The National, Mad Men, and Trader Joe's Cookie Butter."
      images: [
        {
          title: ""
          src: "images/Artists/Kelly Leow/1"
        }
        {
          title: ""
          src: "images/Artists/Kelly Leow/2"
        }
        {
          title: ""
          src: "images/Artists/Kelly Leow/3"
        }
        {
          title: ""
          src: "images/Artists/Kelly Leow/4"
        }
        {
          title: ""
          src: "images/Artists/Kelly Leow/5"
        }
        {
          title: ""
          src: "images/Artists/Kelly Leow/6"
        }
      ]
    }
    brooke_wities: {
      name: "Brooke Wities"
      bio: "Brooke was born with out the ability to speak. After about a year, she conquered that challenge and moved on to her next battle: learning to draw.  Brooke was fortunate enough to have been raised amongst the high quality artistic medium, crayola crayons, at a young age.  Her artistic endeavors and exploration of media progressed, as she grew older.  She enrolled in a wide range of art classes at her public high school, which encouraged art and creativity.  She continued onto UCLA where she co-founded the student organization Art Shaped Box and is currently majoring in Nursing.
            Brooke often found herself paying absolutely no attention in her nursing lectures to the verbal content, but was very focused on the anatomical images. She frequently spends her time in lecture doodling brains, human forms, skeletons, and various organs.  This has played a major role in her artwork, which tends to be anatomically influenced and surreal.
            Friends and family of Brooke think her artwork is very good.  That is saying something since Brooke is surrounded by many critical people.  However, many notable and influential people have never seen her artwork.  Once Christian bale stood 20 feet away from many of her paintings and even almost saw them. He was almost impressed by them."
      images: [
        {
          title: ""
          src: "images/Artists/Brooke Wities/1"
          caption: ""
        }
        {
          title: ""
          src: "images/Artists/Brooke Wities/2"
          caption: ""
        }
        {
          title: ""
          src: "images/Artists/Brooke Wities/3"
          caption: ""
        }
        {
          title: ""
          src: "images/Artists/Brooke Wities/4"
          caption: ""
        }
        {
          title: ""
          src: "images/Artists/Brooke Wities/5"
          caption: ""
        }
        {
          title: ""
          src: "images/Artists/Brooke Wities/6"
          caption: ""
        }
        {
          title: ""
          src: "images/Artists/Brooke Wities/7"
          caption: ""
        }
        {
          title: ""
          src: "images/Artists/Brooke Wities/8"
          caption: ""
        }
      ]
      }
    ying_zhi_lim: {
      name: "Ying Zhi Lim"
      bio: "I do a large variety of art, from painting to photography to crafting, I'd like to think myself as a jack of all trades, utilizing my various skills in many fields. I've sold my works on stores, creating a business out of them while still using art as an outlet for artistic expression."
      images: [
        {
          title: ""
          src: "images/Artists/Ying Zhi Lim/1"
          caption: ""
        }
        {
          title: ""
          src: "images/Artists/Ying Zhi Lim/2"
          caption: ""
        }
        {
          title: ""
          src: "images/Artists/Ying Zhi Lim/3"
          caption: ""
        }
        {
          title: ""
          src: "images/Artists/Ying Zhi Lim/4"
          caption: ""
        }
      ]
    }
  }
      
  
  getTemplateData: ->
    data = @templateData[@person]
    if data.images[0]?
      data.images[0].active = true
    data
    
    
    