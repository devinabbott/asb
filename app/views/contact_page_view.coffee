template = require 'views/templates/contact'
PageView = require 'views/base/page_view'

module.exports = class ContactPageView extends PageView
  template: template
  className: 'contacts-page'
  
  templateData: {
    officers: [ 
      {
        position: 'President'
        info: [{
          name: "Vanessa Goh"
          email: 'vldgoh@ucla.edu'
        }]
      }
      {
        position: 'Vice President'
        info: [{
          name: 'Jessica Lee'
          email: 'jesslee3@ucla.edu'
        }]
      }
      {
        position: 'Treasurer'
        info: [{
          name: 'Brooke Wities'
          email: 'bwities@gmail.com'
        }]
      }
      {
        position: 'Treasurer Committee'
        info: [{
          name: 'Emily Chuang'
          email: 'emilykchuang@gmail.com'
        }
        {
          name: 'Divya Narasimhan'
          email: 'dnarasimhan29@gmail.com'
        }]
      }
      {
        position: 'External'
        info: [{
          name: 'Kelly Leow'
          email: 'kellyawesomeleow@gmail.com'
        }]
      }
      {
        position: 'External Committee'
        info: [{
          name: 'Divya Narasimhan'
          email: 'dnarasimhan29@gmail.com'
        }]
      }
      {
        position: 'Web Management'
        info: [{
          name: "Devin Abbott"
          email: "devinaabbott@gmail.com"
        }
        {
          name: "Cristal Alba"
          email: "cristalgalba@gmail.com"
        }]
      }
    ]
  }

  getTemplateData: ->
    @templateData

    
