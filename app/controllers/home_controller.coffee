Controller = require 'controllers/base/controller'
HomePageView = require 'views/home_page_view'
ArtistPageView = require 'views/artist_page_view'
ContactPageView = require 'views/contact_page_view'
LocationPageView = require 'views/location_page_view'
AboutPageView = require 'views/about_page_view'
CalendarPageView = require 'views/calendar_page_view'
IndividualPageView = require 'views/individual_page_view'

module.exports = class HomeController extends Controller
  historyURL: ''
  
  constructor: ->
    #console.log 'startup controller', arguments
    super

  index: ->
    @view = new HomePageView()

  artists: ->
    @view = new ArtistPageView()
    
  contacts: ->
    @view = new ContactPageView()
    
  location: ->
    @view = new LocationPageView()
    
  about: ->
    @view = new AboutPageView()
    
  calendar: ->
    @view = new CalendarPageView()
    
  individual: ( params ) ->
    #console.log 'individual', arguments
    #console.log 'loading artist', params.name
    @view = new IndividualPageView params.name