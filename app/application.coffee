Chaplin = require 'chaplin'
mediator = require 'mediator'
routes = require 'routes'
Layout = require 'views/layout'

HeaderView = require 'views/header_view'

# The application object
module.exports = class Application extends Chaplin.Application
  # Set your application name here so the document title is set to
  # “Controller title – Site title” (see Layout#adjustTitle)
  title: 'Art Shaped Box'

  initialize: ->
    super

    # Initialize core components
    @initDispatcher()
    @initLayout()
    @initMediator()
    
    @initRouter routes, { pushState: false }
    
    @header = new HeaderView
    @header.render()

  # Override standard layout initializer
  # ------------------------------------
  initLayout: ->
    # Use an application-specific Layout class. Currently this adds
    # no features to the standard Chaplin Layout, it’s an empty placeholder.
    @layout = new Layout {@title}

  # Create additional mediator properties
  # -------------------------------------
  initMediator: ->
    # Create a user property
    Chaplin.mediator.user = null
    # Add additional application-specific properties and methods
    # Seal the mediator
    Chaplin.mediator.seal()
